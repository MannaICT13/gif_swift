//
//  ViewController.swift
//  gif_swift
//
//  Created by Md Khaled Hasan Manna on 27/11/20.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var gifImgView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        gifImgView.loadGif(name: "giphy")
       
    }


}

